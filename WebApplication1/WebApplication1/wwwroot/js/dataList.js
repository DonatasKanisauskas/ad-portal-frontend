﻿class DataList {
	constructor(containerId, inputId, listId, options) {
		this.containerId = containerId;
		this.inputId = inputId;
		this.listId = listId;
		this.options = options;
		this.value = "";
	}

	create(filter = "") {
		const list = document.getElementById(this.listId);
		const filterOptions = this.options.filter(
			d => filter === "" || d.text.toLowerCase().includes(filter.toLowerCase())
		);

		if (filterOptions.length === 0) {
			list.classList.remove("active");
		} else {
			list.classList.add("active");
		}

		list.innerHTML = filterOptions
			.map(o => `<li id=${o.value}>${o.text}</li>`)
			.join("");
	}

	addListeners(datalist) {
		const container = document.getElementById(this.containerId);
		const input = document.getElementById(this.inputId);
		const list = document.getElementById(this.listId);

		window.addEventListener("click", e => {
			if (!document.getElementById(this.containerId).contains(e.target)) {
				container.classList.remove("active");
				input.value = this.value;
			}
		});
		container.addEventListener("click", e => {
			if (e.target.id === this.inputId) {
				container.classList.toggle("active");
				this.value = input.value;
				input.value = "";
			} else if (e.target.id === "datalist-icon") {
				container.classList.toggle("active");
				input.value = input.value;
				input.focus();
			}
		});

		input.addEventListener("input", function (e) {
			if (!container.classList.contains("active")) {
				container.classList.add("active");
			}

			datalist.create(input.value);
		});

		list.addEventListener("click", function (e) {
			if (e.target.nodeName.toLocaleLowerCase() === "li") {
				input.value = e.target.innerText;
				container.classList.remove("active");
			}
		});
	}
}

function lower(str){
	return str.toLowerCase();
}

function cars(json) {
	var list = [];
	var array = {};
	var num = 0;
	json.forEach(obj => {
		num++;
		array = { value: num, text: obj.manufacturer.name };
		list.push(array);
	});
	const markes = new DataList("markelist", "marke-input", "marke-ul", list);

	markes.create();
	markes.addListeners(markes);

	//===================================
	list = [];
	array = {};
	num = 0;
	json.forEach(obj => {
		num++;
		array = { value: num, text: obj.manufacturerModel.name };
		list.push(array);
	});
	const modeliai = new DataList("modellist", "model-input", "model-ul", list);

	modeliai.create();
	modeliai.addListeners(modeliai);
}